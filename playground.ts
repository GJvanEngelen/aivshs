await page.route('http://localhost:3000/discount?age=25', (route) => {
    route.fulfill({
      status: 200,
      body: JSON.stringify({ discount: 0.2 }),
    });
});

// Check for alert message
page.on('dialog', async dialog => {
  expect(dialog.message()).toBe('Failed to fetch discount');


  test('should display validation error for negative age', async ({ page }) => {
    await page.fill('#age', '-1');
    await page.click('button[type="submit"]');
    // Check for validation message
    const validationMessage = await page.$eval('#age', (element) => {
      if (element instanceof HTMLInputElement) {
        return element.validationMessage;
      }
      return '';
    });
    expect(validationMessage).toContain('Value must be greater than or equal to 0.');
  });