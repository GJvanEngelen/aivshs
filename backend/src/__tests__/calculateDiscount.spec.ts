// How can I unit test the following code using full boundary analysis?
// The function is positioned at ../calculateDiscount

// export function calculateDiscount(age: number): number {
//     let discount: number;

//     if (age < 0) {
//         throw new Error("Age cannot be negative");
//     } else if (age < 18) {
//         discount = 0.5; // 50% discount for children under 18
//     } else if (age >= 60) {
//         discount = 0.3; // 30% discount for seniors 60 and above
//     } else {
//         discount = 0; // No discount for others
//     }

//     return discount;
// }

