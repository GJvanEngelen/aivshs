// Can you write supertest integration tests for this router, using the condition test test design technique?
// App is positioned at ../app.ts and router at ../discountRouter.ts

// //discountRouter.ts
// import express from 'express';
// import { calculateDiscount } from './calculateDiscount';

// const router = express.Router();

// router.get('/discount', (req, res) => {
//     const ageParam = req.query.age;

//     if (typeof ageParam !== 'string') {
//         return res.status(400).json({ error: 'Age parameter is required and must be a string' });
//     }

//     const age = parseInt(ageParam, 10);

//     if (isNaN(age) || age < 0) {
//         return res.status(400).json({ error: 'Age parameter must be a non-negative integer' });
//     }

//     try {
//         const discount = calculateDiscount(age);
//         res.json({ age, discount });
//     } catch (error: any) {
//         res.status(500).json({ error: error.message });
//     }
// });

// //app.ts
// export default router;

// import express from 'express';
// import cors from 'cors';
// import discountRouter from './discountRouter';

// const app = express();
// const port = 3000;

// // Enable CORS for all routes
// app.use(cors());

// app.use(express.json());
// app.use('/', discountRouter);

// const server = app.listen(port, () => {
//     console.log(`Server is running on http://localhost:${port}`);
// });

// export { app, server };

// test/discountRouter.test.ts
