import express from 'express';
import { calculateDiscount } from './calculateDiscount';

const router = express.Router();

router.get('/discount', (req, res) => {
    const ageParam = req.query.age;

    if (typeof ageParam !== 'string') {
        return res.status(400).json({ error: 'Age parameter is required and must be a string' });
    }

    const age = parseInt(ageParam, 10);

    if (isNaN(age) || age < 0) {
        return res.status(400).json({ error: 'Age parameter must be a non-negative integer' });
    }

    try {
        const discount = calculateDiscount(age);
        res.json({ age, discount });
    } catch (error: any) {
        res.status(500).json({ error: error.message });
    }
});

export default router;
