import express from 'express';
import cors from 'cors';
import discountRouter from './discountRouter';

const app = express();
const port = 3000;

// Enable CORS for all routes
app.use(cors());

app.use(express.json());
app.use('/', discountRouter);

const server = app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});

export { app, server };
