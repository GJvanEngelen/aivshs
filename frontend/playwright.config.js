// playwright.config.js
module.exports = {
    use: {
      baseURL: 'http://localhost:8080', // Ensure your app is running on this URL
      headless: false, // Set to true if you want to run tests in headless mode
    },
  };