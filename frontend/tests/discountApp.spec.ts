// Write playwright tests to test this page using the State Transition Testing.
// The config for the base URL is in playwright.config.js. The project is in typescript.
// //App.vue
// <template>
//   <div id="app">
//     <header>
//       <h1>Get Your Discount!</h1>
//     </header>
//     <main>
//       <form @submit.prevent="getDiscount">
//         <label for="age">Enter your age:</label>
//         <input type="number" v-model="age" id="age" required min="0" />
//         <button type="submit">Calculate Discount</button>
//       </form>
//       <div v-if="discount !== null" class="result">
//         <p>Your discount is: {{ discount * 100 }}%</p>
//       </div>
//     </main>
//   </div>
// </template>

// <script>
// export default {
//   data() {
//     return {
//       age: null,
//       discount: null,
//     };
//   },
//   methods: {
//     async getDiscount() {
//       try {
//         const response = await fetch(`http://localhost:3000/discount?age=${this.age}`);
//         if (!response.ok) {
//           throw new Error('Error fetching discount');
//         }
//         const data = await response.json();
//         this.discount = data.discount;
//       } catch (error) {
//         console.error(error);
//         alert('Failed to fetch discount');
//       }
//     },
//   },
// };
// </script>

// tests/discount.spec.js

// tests/discount.spec.ts
